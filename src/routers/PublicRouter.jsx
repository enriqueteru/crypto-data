
import { Routes, Route } from "react-router-dom";
import {
  UserInfo,
  FavoritesCrypto,
  CryptoAssetsList,
  NotFoundPage,
} from "../pages";
import CryptoRouter from "./CryptoRouter";


const PublicRouter = ({ data, loading }) => {
  
  

  return (
    <>
      
        <Routes>
          <Route path="/*" element={<NotFoundPage />} />

          <Route
            path="/"
            element={<CryptoAssetsList data={data} loading={loading} />}
          />
          <Route
          path="/fav"
          element={<FavoritesCrypto />}
        />

          <Route path="/user" element={<UserInfo />} />

          <Route path="/crypto/*" element={<CryptoRouter />} />
        </Routes>
    
    </>
  );
};

export default PublicRouter;
