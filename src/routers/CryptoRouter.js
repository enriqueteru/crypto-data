import React from "react";
import { Routes, Route } from "react-router-dom";
import {CryptoDetails, NotFoundPage, SearchPage} from '../pages'

const CryptoRouter = () => {
  return (
    <>
      <Routes>

      <Route
          path="/*"
          element={<NotFoundPage />}
        />

         <Route
          path="/search/:search"
          element={<SearchPage />}
        />
         
         <Route
          path="/search/"
          element={<SearchPage />}
        />
         
  
        <Route
          path="/:id"
          element={<CryptoDetails />}
        />

        

      </Routes>
    </>
  );
};


export default CryptoRouter;