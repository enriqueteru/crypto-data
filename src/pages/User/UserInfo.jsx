import React, { useContext } from "react";
import { DataContext } from "../../contexts/DataContext";

const UserInfo = () => {
  const userData = useContext(DataContext);
  const { name, subname, since, description, avatar } = userData;
  return (
    <div style={{display:'flex', flex:'row', gap:'50px', margin:'120px'}}>
    <img style={{width:'500px'}} src={avatar} alt={subname} />
     
     <div>
     <h1 className="theme__title">  Profile Info </h1>
      <h3> {name} {subname}</h3>
      <p>Register since: {since}</p>
      
      <p>{description}</p>
      </div>


    </div>
  );
};

export default UserInfo;
