import { Link, useParams } from "react-router-dom";
import React, { useEffect, useState } from "react";
import axios from "axios";


const SearchPage = () => {
  const key = "74d547bd8c383b700a0dbdcf7e31194e2ee5604b";
  const url = `https://api.nomics.com/v1/currencies/ticker?key=${key}&interval=1d&convert=EUR&per-page=1000`;

  const [result, setResult] = useState("");

  const params = useParams();
  const { search } = params;

  useEffect(() => {
    async function getData() {
      let response = await axios(url);
      const { data } = response;
      console.log({ data });
      console.log(search);
      const resultDataFilter = await data.filter(
        (coin) =>
          coin.id.toLowerCase().includes(search.toLocaleLowerCase()) ||
          coin.name.toLowerCase().includes(search.toLocaleLowerCase())
      );
      setResult(resultDataFilter);
      console.log(result);
    }

    getData();
  }, [search]);

  return (
    <>
      {result.length <= 0 && <p> {search} not found any crypto asset </p>}

      {result.length >= 1 && (
        <div className="searchpage">
          <h1 className="searchpage__title">
            <span>{result.length}</span> <small style={{fontSize:'15px'}}>results for</small> <i>{search}</i>
          </h1>
          <table className="searchpage__table">
            <tr>
              <th>Logo</th>
              <th>Symbol</th>
              <th>Name</th>
              <th>Price</th>
            </tr>
            {result.map((coin) => (
              <tr>
                <th>
                  <img style={{ height: "20px" }} src={coin.logo_url} alt="" />
                </th>
                <th>
                  <Link to={`/crypto/${coin.id}`}>
                    <p className="theme__p"> {coin.id} </p>
                  </Link>
                </th>
                <th>
                  <p> {coin.name}</p>
                </th>
                <th>
                  <p> {coin.price}</p>
                </th>
              </tr>
            ))}
          </table>
        </div>
      )}

      {search === undefined && <p> Not query</p>}
    </>
  );
};

export default SearchPage;
