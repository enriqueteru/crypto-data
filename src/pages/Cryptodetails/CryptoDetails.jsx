import { useParams } from "react-router-dom";
import { CryptoBanner, CryptoInfo } from "..";
import { Loader, Rank } from "../../components";
import { useFetch } from "../../hooks/useFecth";

const CryptoDetails = () => {
  const params = useParams();
  const { id } = params;

  const key = "74d547bd8c383b700a0dbdcf7e31194e2ee5604b";
  const url = `https://api.nomics.com/v1/currencies/ticker?key=${key}&ids=${id.toUpperCase()}`;
  const { data, loading } = useFetch(url);
  return (
    <>

 {data && (

        <div className="cryptodetails">

{loading && <Loader />}
          <div className="cryptodetails__row1">
            <CryptoBanner data={data} />
          </div>

          <div className="cryptodetails__row2">
            <div className="cryptodetails__card">
              <Rank data={data} />
              <CryptoInfo data={data} />
            </div>
          </div>
        </div>
      )}

      
    </>
  );
};

export default CryptoDetails;
