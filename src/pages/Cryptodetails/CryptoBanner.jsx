import React from "react";
import { Link } from "react-router-dom";
import { IoReturnDownBackOutline } from "react-icons/io5";


const CryptoBanner = ({ data }) => {
  return (
    <div>
     
      <div className="cryptodetails__card-horizontal">
     
   
        <img className="cryptodetails__img" src={data[0].logo_url} alt="logo" />
        <div>
          <h1 className="cryptodetails__title">{data[0].name}</h1>
          <p className="theme__p">({data[0].symbol})</p>
          <h1 className="theme__subtitle">{data[0].price}€</h1>
        </div>
      </div>

      <div className="cryptodetails__card-vertical">

      <img style={{width:'400px', height:'auto', margin:'80px 0'}} src="/img/charts_demo.png" alt="charts" />


      <Link className="theme__btn" to="/">
                See more cryptos
              </Link>
              {/* <Link className="theme__btn" to="/">
     <p>   <IoReturnDownBackOutline />  Return </p>
              </Link> */}
      
      </div>
    </div>
  );
};

export default CryptoBanner;
