import React from 'react'

const CryptoInfo = ({data}) => {
  return (<>
    <div style={{marginTop:'80px'}} >
    <h1 className="cryptodetails__subtitle" >Price Change</h1>
    <p className={data[0].ytd.price_change >= 0 ? 'green theme__p' : 'red' }>{data[0].ytd.price_change}</p>
    </div>
    <div>
    <h1 className="cryptodetails__subtitle">Market cap</h1>
    <p className="theme__p">{data[0].market_cap}</p>
    </div>
    <div>
    <h1 className="cryptodetails__subtitle">Exchanges #</h1>
    <p className="theme__p">{data[0].num_exchanges}</p>
    </div>
    <div>
    <h1 className="cryptodetails__subtitle">circulating Supply</h1>
    <p className="theme__p">{data[0].circulating_supply}</p>
    </div>
    <div>
    <h1 className="cryptodetails__subtitle">circulating Supply</h1>
    <p className="theme__p">{data[0].circulating_supply}</p>
    </div>
    <div>
    <h1 className="cryptodetails__subtitle">High</h1>
    <p className="theme__p">{data[0].high}</p>
    </div>
    <div>
    <h1 className="cryptodetails__subtitle">Pairs #</h1>
    <p className="theme__p">{data[0].num_pairs}</p>
    </div> 
    </>
  )
}

export default CryptoInfo