import { useNavigate } from "react-router-dom";
import IsFav from "../../components/IsFav";

const CryptoAssetItem = ({ data }) => {

  
  const navigate = useNavigate();
  const fetchData = data;
  return (
    <>
      {fetchData.map(({ id, logo_url, name, price }) => (
        <div className="assetslists__grid-item" key={id}>
          <div style={{ display: "flex", alignItems: "center" }}>
            <IsFav id={id} />
            <img
              className="assetslists__grid-item-logo"
              src={logo_url}
              onClick={() => {
                navigate(`/crypto/${id.toUpperCase()}`);
              }}
              alt={id}
            />

            <div>
              <h2 className="assetslists__grid-item-p"> {name} </h2>
              <p className="theme__p"> ({id}) </p>
              <p className="assetslists__grid-item-p"> {price} </p>
              <button
                onClick={() => {
                  navigate(`/crypto/${id.toUpperCase()}`);
                }}
                className="theme__btn-dissable"
              >
                See details
              </button>
            </div>
          </div>
        </div>
      ))}
    </>
  );
};

export default CryptoAssetItem;
