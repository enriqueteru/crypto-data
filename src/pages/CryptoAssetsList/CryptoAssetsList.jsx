import { useState } from "react";
import { Loader, Paginator } from "../../components";
import { CryptoAssetItem } from '../index'
import { useFetch } from "../../hooks/useFecth";

const CryptoAssetsList = () => {
  let [actualPage, setActualPage] = useState(1);

  if (actualPage === 0) {
    setActualPage(1);
  }

  const key = "74d547bd8c383b700a0dbdcf7e31194e2ee5604b&ids&ids";
  const url = `https://api.nomics.com/v1/currencies/ticker?key=${key}&interval=1d&convert=EUR&per-page=12&page=${actualPage}`;
  const { data, loading } = useFetch(url);
  
  return (
    <div className="animate__animated animate__fadeIn assetslists">
      <h1  style={{paddingTop:'40px'}} className="theme__title" > Assets list</h1>
      <div className="animate__animated animate__fadeIn assetslists__grid">
        {data && <CryptoAssetItem data={data} />}

        {loading && (
         <Loader />
        )}

        <Paginator actualPage={actualPage} setActualPage={setActualPage} />
      </div>
    </div>
  );
};

export default CryptoAssetsList;
