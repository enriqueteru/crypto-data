import React from 'react'
import { useContext } from 'react'
import { FavContext } from '../../contexts'
const FavoritesCrypto = () => {


  const favData = useContext(FavContext);
  const {favListState} = favData;
  console.log(favListState)


  return (<>
    <h1 className="theme__title"> Favorites Assets</h1>
    {favListState.map(el => <p> {el} </p>)
}
</>

  )
}

export default FavoritesCrypto