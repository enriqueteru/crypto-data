import React from 'react';
import { Link } from 'react-router-dom';

const NotFoundPage = () => {
  
  return (
    <div className='notfound'>
              <Link className='theme__btn  notfound__btn' to='/'> Go to homepage  </Link> 
        <h1 className='theme__title notfound__title '> Not Found </h1>
        <img className='notfound__img' src="img/page-not-found.gif"  alt="404"/>

    </div>
  )
}

export default NotFoundPage