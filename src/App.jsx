import { DataContext, FavContext } from "./contexts";

import { userData } from "./helpers/userData";

import PublicRouter from "./routers/PublicRouter";
import { NavHeader } from "./components";

import "animate.css";
import { useEffect, useReducer, useState } from "react";
import { favReducer } from "./reducers/favReducer";


function App() {

  const [favListState, dispatch] = useReducer(favReducer, []);

  useEffect(() => {
   localStorage.setItem('fav', favListState)
  
  }, [favListState])
  

  const handleDelete = (id) =>  {
  
    const action = {
      type : 'DELETE_FAV',
      payload: id
    }

    dispatch(action)
  }


  const handleAdd = (id) => { 

    const action = { 
      type: 'ADD_FAV',
      payload: id
    }
     dispatch(action);
    
  }




  return (
    <>
      <FavContext.Provider value={{favListState, dispatch, handleAdd, handleDelete, }}>
      <DataContext.Provider value={userData}>
        <NavHeader />
        <div className="assetslists">
          <PublicRouter />
        </div>
      </DataContext.Provider>
      </FavContext.Provider>
    </>
  );
}

export default App;
