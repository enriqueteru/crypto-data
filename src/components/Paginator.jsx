import React from 'react'

const Paginator = ({actualPage, setActualPage}) => {
  return (
    <div className="assetslists__grid-paginator">
            <button
              className="theme__btn"
              onClick={() => {
                setActualPage(actualPage - 1);
              }}
            >
              Prev
            </button>
            <span>{actualPage}</span>
            <button
              className="theme__btn"
              onClick={() => {
                setActualPage(actualPage + 1);
              }}
            >
              {" "}
              next
            </button>
            </div>
  )
}

export default Paginator