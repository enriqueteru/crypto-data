import React from "react";
import { NavLink } from "react-router-dom";

const links = [
  {
    name: "Assets",
    url: "/",
  },
  {
    name: "Fav",
    url: "/fav",
  },
];

const NavItems = () => {
  return (
    <>
      {links.map(({ name, url }) => {
        return (
          <NavLink key={name} className="nav__items-link" 
          to={url}
          >
            {name}
          </NavLink>
        );
      })}
    </>
  );
};

export default NavItems;
