import { useContext } from "react";
import { Link } from "react-router-dom";
import { NavItems, SearchComponent } from "../../components";
import { DataContext } from "../../contexts/DataContext";

const NavHeader = () => {
  const userData = useContext(DataContext);
  const { name, avatar } = userData;
  return (
    <div className="nav">
      <div className="nav__items">
        <Link to="/">
          {" "}
          <img className="nav__logo" src="/img/logo.svg" alt="logo" />{" "}
        </Link>
        <NavItems />
        <SearchComponent />
      </div>

      <div className="nav__user">
        <Link to="/user">
          {" "}
          <img className="nav__user-img" src={avatar} alt="user-avatar" />{" "}
        </Link>
        <Link className="nav__items-link" to="/user">
          {" "}
          {name}
        </Link>
      </div>
    </div>
  );
};

export default NavHeader;
