import React from 'react'

const Loader = () => {
  return (
     <>
    <img
    style={{ margin: "50px" }}
    className="loader"
    src={"/img/loader.gif"}
    alt="loader"
  />
  <p>Loading awesome stuff</p>
  </>
  )
}

export default Loader