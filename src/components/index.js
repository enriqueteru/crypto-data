import NavHeader from "./Nav/NavHeader";
import NavItems from './Nav/NavItems';
import Paginator from "./Paginator";
import Rank from "./Rank";
import Loader from './Loader'
import SearchComponent from "./SearchComponent";
import IsFav from "./IsFav";
export {
    IsFav,
    Loader,
    NavHeader,
    NavItems,
    Paginator,
    Rank,
    SearchComponent,
}