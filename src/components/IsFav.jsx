import { useContext, useState } from "react";
import { AiFillHeart, AiOutlineHeart } from "react-icons/ai";
import { FavContext } from "../contexts";



const IsFav = ({ id }) => {

   const [ isFav, setIsFav] = useState(false)
   const FavData = useContext(FavContext);
   const{handleAdd, handleDelete} = FavData



 
  return (
    <>
      {!isFav && <AiOutlineHeart 
      style={{ cursor: "pointer"}}
      onClick={()=> {
          setIsFav(true);
          handleAdd(id);
          
    
    }
}
      />}

      {isFav  && 
      <AiFillHeart 
      style={{ cursor: "pointer" }} 
      onClick={()=> {
        setIsFav(false);
        handleDelete(id);
    }}

      />}

    </>
  );
};

export default IsFav;
