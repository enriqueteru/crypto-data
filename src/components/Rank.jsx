import React from 'react'

const Rank = ({data}) => {
  return (
    <h1 className="theme__title"> World Rank #{data[0].rank}</h1>
  )
}

export default Rank