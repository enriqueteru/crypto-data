import React, { useState } from "react";
import { IoSearch, IoClose } from "react-icons/io5";
import { useNavigate } from "react-router-dom";
import { useForm } from "../hooks/useForm";

const SearchComponent = () => {
  const [searchInputVisible, setSearchInputVisible] = useState(false);

  const initialForm = {
    search: "",
  };

  const [formValues, handleInputChange, reset] = useForm(initialForm);
  const navigate = useNavigate();

  const handleSubmitForm = (ev) => {
    ev.preventDefault();
    console.log(ev);
    navigate(`/crypto/search/${formValues.search}`);
  };

  return (
    <>
      <form onSubmit={handleSubmitForm} className="nav__form">
        {searchInputVisible === false && (
          <IoSearch
            className="hoverable"
            onClick={() => {
              setSearchInputVisible(!searchInputVisible);
            }}
          />
        )}

        {searchInputVisible && (
          <>
            <input
              name="search"
              onChange={handleInputChange}
              value={formValues.search}
              className="nav__form-input animate__animated animate__fadeIn"
              type="text"
            />
            <IoSearch
              className="hoverable"
              onClick={() => {
                navigate(`/crypto/search/${formValues.search}`);
              }}
            />
            <IoClose
              className="hoverable"
              onClick={() => {
                setSearchInputVisible(!searchInputVisible);
              }}
            />
          </>
        )}
      </form>
    </>
  );
};

export default SearchComponent;
